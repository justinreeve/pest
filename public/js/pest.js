var categories;
var pests;
$.get('/api/categories', function(data)
{
	categories = data;
}).done(function()
{
});

$(document).ready(function()
{
	var categorySelect = $('select[name="categoryId"]');
	var pestSelect = $('select[name="pestId"]');
	var pestBlock = $('#pestBlock');

	categorySelect.on('change', function()
	{
		var selectedCategoryId = $(this).val();
		var selectedPest;

		for (c = 0; c < categories.length++; c++)
		{
			if (categories[c]['id'] == selectedCategoryId)
			{
				var cat = categories[c];

				// Add the dropdown.
				var html = '<div id="pestSelectBlock" class="formBlock"><div class="divHeader">Pest</div><div><select name="pestId">';
				for (p = 0; p < cat['pests'].length; p++)
				{
					var pest = cat['pests'][p];
					html += '<option value="' + pest['id'] + '">' + pest['name'] + '</option>';
				}
				html += '</select></div>';
				selectedPest = cat['pests'][0];

				// Add the image.
				if (selectedPest['imageFile'])
				{
					html += '<div id="pestImageBlock" class="formBlock"><img src="/images/pests/' + selectedPest['imageFile'] + '" alt="" /></div>';
				}

				// Add the report button.
				html += '<div id="pestReportBlock" class="formBlock"> \
							<input class="fullsize" type="submit" value="Report This Pest" /> \
						</div>';
				pestBlock.html(html);

				selectedPest['description'] = striptags(selectedPest['description']);
				selectedPest['remedy'] = striptags(selectedPest['remedy']);
				selectedPest['description'] = selectedPest['description'].replace(/(?:\r\n|\r|\n)/g, '<br />');
				selectedPest['remedy'] = selectedPest['remedy'].replace(/(?:\r\n|\r|\n)/g, '<br />');

				// Add the description and remedy.
				var pestContentBlock = $('#pestContentBlock');
				html = '<h6>' + selectedPest['name'] + '</h6> \
						<div class="description">' + selectedPest['description'] + '</div> \
						<h6>Suggested Remedy</h6> \
						<div class="remedy">' + selectedPest['remedy'] + '</div>';
				pestContentBlock.html(html);
			}
		}
	});
	
	$('#pestBlock').on('change', 'select[name="pestId"]', function()
	{
		var pestBlock = $('#pestBlock');
		var selectedPestId = $(this).val();
		for (c = 0; c < categories.length++; c++)
		{
			var cat = categories[c];
			for (p = 0; p < cat['pests'].length; p++)
			{
				var pest = cat['pests'][p];
				if (pest['id'] == selectedPestId)
				{
					var selectedPest = pest;

					// Change the image.
					var html = '';
					var pestImageBlock = $('#pestImageBlock');
					if (selectedPest['imageFile'])
					{
						html = '<div id="pestImageBlock" class="formBlock"><img src="/images/pests/' + selectedPest['imageFile'] + '" alt="" /></div>';
						pestImageBlock.html(html);
					}
					else
					{
						pestImageBlock.html('');
					}

					selectedPest['description'] = striptags(selectedPest['description']);
					selectedPest['remedy'] = striptags(selectedPest['remedy']);
					selectedPest['description'] = selectedPest['description'].replace(/(?:\r\n|\r|\n)/g, '<br />');
					selectedPest['remedy'] = selectedPest['remedy'].replace(/(?:\r\n|\r|\n)/g, '<br />');

					// Change the description and remedy.
					var pestContentBlock = $('#pestContentBlock');
					html = '<h6>' + selectedPest['name'] + '</h6> \
							<div class="description">' + selectedPest['description'] + '</div> \
							<h6>Suggested Remedy</h6> \
							<div class="remedy">' + selectedPest['remedy'] + '</div>';
					pestContentBlock.html(html);
					return;
				}
			}
		}
	});
});

function striptags(html)
{
	var tmp = document.createElement('div');
	tmp.innerHTML = html;
	var txt = tmp.textContent || tmp.innerText;
	return txt;
}
