<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
            $table->increments("id",true);
            $table->string("username")->nullable()->default(null);
            $table->string("password")->nullable()->default(null);
            $table->string("email")->nullable()->default(null);
            $table->string("fullname")->nullable()->default(null);
            $table->string("location")->nullable()->default(null);
            $table->string("location_desc")->nullable()->default(null);
            $table->integer("access")->default(0);
			$table->string('remember_token')->nullable()->default(null);
            $table->timestamps();

			// Indexes.
			$table->unique('username');
			$table->index('location');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}