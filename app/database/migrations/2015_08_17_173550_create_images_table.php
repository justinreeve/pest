<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function(Blueprint $table)
		{
			$table->increments('id');

			// Issue fields.
			$table->integer('issue_id');
			$table->string('hash');
			$table->string('filename');
			$table->integer('filesize');
			$table->integer('sequence')->nullable();

			$table->timestamps();

			// Indexes.
			$table->index('issue_id');
			$table->unique(array('issue_id', 'hash', 'filesize'));
			$table->unique(array('issue_id', 'filename'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('images');
	}
}
