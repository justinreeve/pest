<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id');
			$table->string('name', 32);
			$table->text('description')->nullable();;
			$table->text('remedy')->nullable();;
			$table->string('imageFile')->nullable();
			$table->string('externalUrl')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pests');
	}

}
