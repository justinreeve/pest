<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issues', function(Blueprint $table)
		{
			$table->increments('id');

			// Issue fields.
			$table->integer('pest_id');
			$table->integer('submitter_id');
			$table->integer('location_id');
			$table->timestamp('date_sighted');
			$table->integer('sighting_location_id');
			$table->integer('notified_by');
			$table->text('location_details')->nullable();
			$table->text('notes')->nullable();

			// Resolution fields.
			$table->boolean('resolved')->nullable();
			$table->string('status', 16)->nullable();
			$table->timestamp('date_completed')->nullable();
			$table->integer('action_id')->nullable();
			$table->text('resolution_notes')->nullable();
			$table->decimal('expenses', 8, 2)->nullable();
			$table->integer('chemical_id')->nullable();
			$table->timestamp('date_applied')->nullable();
			$table->integer('location_applied')->nullable();
			$table->integer('applicator')->nullable();
			$table->text('admin_notes')->nullable();
			$table->string('work_order', 32)->nullable();
			$table->boolean('emailed_resolution')->default(false);
			$table->boolean('emailed_notification')->default(false);

			$table->timestamps();

			// Indexes.
			$table->index('submitter_id');
			$table->index('location_id');
			$table->index('sighting_location_id');
			$table->index('notified_by');
//			$table->index('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issues');
	}

}
