<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function showLogin()
	{
		return View::make('login');
	}

	public function doLogin()
	{
		// validate the info, create rules for the inputs
		$rules = array(
		    'username' => 'required',
		    'password' => 'required',
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
		    return Redirect::to('login')
		        ->withErrors($validator) // send back all errors to the login form
		        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
		
			$username = Input::get('username');
			$password = Input::get('password');

			
		    // Attempt to do the login.
		    if (Corp::auth($username, $password))
			{
				// See https://github.com/stevebauman/Corp
				$ldapuser = Corp::user($username);
				$adldapuser = Corp::adldap()->user()->info($username);
				$location_desc = $adldapuser[0]['department'][0];

				$user = User::where('username', $username)->first();
				if (!$user)
				{
					$userData = array(
						'username' => $ldapuser->username,
						'password' => '',
						'email' => $ldapuser->email,
						'fullname' => $ldapuser->name,
						'location' => $ldapuser->group,
						'location_desc' => $location_desc,
						'access' => 0,
					);
					$user = new User($userData);
					$user->save();
				}

				if ($user && isset($user->id))
				{
					Auth::login($user);
				}
				return Redirect::to('/');
			}
			else
			{
				// Try authenticating non-LDAP.
				$user = User::where('username', $username)
						->where('password', $password)
						->where('password', '!=', '')
						->first();
				if ($user && isset($user->id))
				{
					Auth::login($user);
					return Redirect::to('/');
				}
				else
				{
			        // validation not successful, send back to form
			        return Redirect::to('login')->with('ldaperrormessage', 'Username/password was incorrect. Please try again.');
				}
		    }
		
		}
	}

	public function doLogout()
	{
		Auth::logout();
		return Redirect::to('login');
	}
}
