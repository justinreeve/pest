<?php

class PestController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = Category::lists('name', 'id');

		$params = array(
			'categories' => $categories
		);

		return View::make('issue', $params);
	}


	public function getCategoriesJSON()
	{
		$categories = Category::with('pests')->get();
		return $categories;
	}


	/**
	 * @return Response
	 */
	protected function emailNewIssue($issueId)
	{
		$issue = Issue::with('pest')->with('submitter')->find($issueId);

		// Don't email if a notification has already been sent.
		if ($issue->emailed_notification == true)
			return;

		$data = array(
			'issue_id' => $issue->id,
			'pest' => $issue->pest->name,
			'date_sighted' => $issue->date_sighted,
			'submitter' => $issue->submitter->fullname,
			'location_desc' => $issue->submitter->location_desc
		);

		Mail::send('emails.newissuereported', $data, function($message)
		{
			$message->from('donotreply@wsd.net', 'IPM Management');
			if (Config::get('app.debug') === false && Config::get('app.url') != 'http://pest.wsd.dev')
				$message->to('boydharris@wsd.net')->cc('daparker@wsd.net')->cc('dwiddison@wsd.net')->cc('amchristensen@wsd.net')->cc('jreeve@wsd.net');
			else
				$message->to('jreeve@wsd.net');
			$message->subject('New IPM Issue');
		});
		$issue->emailed_notification = true;
		$issue->save();
		return;
	}


	/**
	 * @return Response
	 */
	protected function emailIssueResolved($issueId)
	{
		$issue = Issue::with('pest')->with('submitter')->with('action')->find($issueId);

		// Make sure the issue is actually resolved, and a notification hasn't already been sent.
		if (!isset($issue->date_completed) || (bool) strtotime($issue->date_completed) == false
			|| strtotime($issue->date_completed) == 0 || $issue->status != 'resolved'
			|| $issue->emailed_resolution == true)
			return;

		$data = array(
			'issue_id' => $issue->id,
			'pest' => $issue->pest->name,
			'date_created' => $issue->created_at,
			'date_sighted' => $issue->date_sighted,
			'date_completed' => $issue->date_completed,
			'action' => $issue->action->name,
			'resolution_notes' => $issue->resolution_notes,
		);

		Mail::send('emails.issueresolved', $data, function($message) use ($issue)
		{
			$message->from('donotreply@wsd.net', 'IPM Management');
			$message->to($issue->submitter->email)->cc('jreeve@wsd.net');
			$message->subject('Your pest issue has been resolved');
		});

		$issue->emailed_resolution = true;
		$issue->save();
		return;
	}


	/**
	 * We'll want to retrieve a list of locations so users (access level 50) can only see pests at their
	 * specific location. Administrators (access level 100) should be able to select any locations, or
	 * see all reported issues.
	 *
	 * @return Response
	 */
	public function listIssues($locationId = null)
	{
		if ($locationId == 'all')
			$locationId = null;

		$location_results = DB::select('SELECT DISTINCT location, location_desc FROM users ORDER BY location_desc');
		$locations = array('all' => 'All');
		foreach ($location_results as $lr)
		{
			$locations[$lr->location] = $lr->location_desc;
		}

		$user = Auth::user();
		if ($user->access < 100)
		{
/*
			$issues = Issue::with('pest')
							->with('submitter')
							->where('users.location', '=', $user->location)
							->orderBy('date_sighted', 'desc')
							->get();
*/
			// Filter out just the issues at the user's location.
			$iss = Issue::with('pest')
							->with('submitter')
							->orderBy('date_sighted', 'desc')
							->get();

			$issues = array();
			foreach ($iss as $issue)
			{
				if ($issue->location_id == $user->location)
					$issues[] = $issue;
			}

		}
		else
		{
			if ($locationId)
			{
				$issues = Issue::with('pest')
								->with('submitter')
								->where('location_id', '=', $locationId)
								->orderBy('date_sighted', 'desc')
								->get();
			}
			else
			{
				$issues = Issue::with('pest')
								->with('submitter')
								->orderBy('date_sighted', 'desc')
								->get();
			}
		}

		$params = array(
			'locations' => $locations,
			'locationId' => isset($locationId) ? $locationId : 'all',
			'issues' => $issues,
		);

		return View::make('issuelist', $params);
	}


	/**
	 * @return Response
	 */
	public function showInfo($categoryId = null)
	{
		$categories = Category::lists('name', 'id');
		$categories = array_merge(array('0' => 'Please select one...'), $categories);

		$params = array(
			'categories' => $categories,
			'categoryId' => 0,
		);

		return View::make('issue', $params);
	}


	/**
	 * @return Response
	 */
	public function showInfoCategory($categoryId = null, $pestId = null)
	{
		$categories = Category::lists('name', 'id');
		$categories = array_merge(array('0' => 'Please select one...'), $categories);
		$pests = Pest::where('category_id', $categoryId)->orderBy('name')->lists('name', 'id');

		if ($pestId != null)
		{
			// Make sure the $pestId actually belongs to the category.
			$pest = Pest::where('category_id', $categoryId)
						->where('id', $pestId)
						->where('deleted', 0)
						->first();
		}

		if (!isset($pest))
			$pest = Pest::where('category_id', $categoryId)->where('deleted', 0)->first();

		// Get the issue fields.
		$sightingLocations = SightingLocation::lists('name', 'id');
		$sightingLocationId = reset($sightingLocations);

		$resolutionActions = Action::lists('name', 'id');
		$resolutionActionId = reset($resolutionActions);

		if (isset($pest->description)) $pest->description = strip_tags($pest->description);
		if (isset($pest->remedy)) $pest->remedy = strip_tags($pest->remedy);

		$params = array(
			'categories' => $categories,
			'categoryId' => $categoryId,
			'pestId' => $pestId,
			'pests' => $pests,
			'pest' => $pest,
			'sightingLocations' => $sightingLocations,
			'sightingLocationId' => $sightingLocationId,
			'resolutionActions' => $resolutionActions,
			'resolutionActionId' => $resolutionActionId,
			'user' => Auth::user(),
		);

		return View::make('newpest', $params);
	}


	/**
	 * @return Response
	 */
	public function redirectToNewIssue()
	{
		// Make sure a "pestId" was POSTed. If not, redirect to the first page.
		$data = Input::all();
		if (isset($data['pestId']))
		{
			return Redirect::to('/issue/new/' . $data['pestId']);
		}
		else
		{
			return Redirect::to('/');
		}
	}

	/**
	 * @return Response
	 */
	public function newIssue($pestId)
	{
		// Get the pest and then the category.
		$pest = Pest::where('id', $pestId)
					->first();
		$categories = Category::lists('name', 'id');
		$categories = array_merge(array('0' => 'Please select one...'), $categories);
		$categoryId = $pest['category_id'];
		$pests = Pest::where('category_id', $categoryId)
						->orderBy('name')
						->lists('name', 'id');

		// Get the issue fields.
		$sightingLocations = SightingLocation::orderBy('name')->lists('name', 'id');
		$sightingLocationId = reset($sightingLocations);

		$user = Auth::user();
		$buildingLocations = BuildingLocation::orderBy('name')->lists('name', 'id');
		$buildingLocationId = $user->location;

		$notifiers = Notifier::lists('name', 'id');
		$notifierId = reset($notifiers);

		$resolutionActions = Action::lists('name', 'id');
		$resolutionActionId = reset($resolutionActions);

		$chemicals = Chemical::orderBy('name')->lists('name', 'id');
		$chemicalId = 0;

//		$locationsApplied = LocationApplied::lists('name', 'id');
//		$locationAppliedId = reset($locationsApplied);

		if (isset($pest->description)) $pest->description = strip_tags($pest->description);
		if (isset($pest->remedy)) $pest->remedy = strip_tags($pest->remedy);

		$params = array(
			'categories' => $categories,
			'categoryId' => $categoryId,
			'pestId' => $pestId,
			'pests' => $pests,
			'pest' => $pest,
			'buildingLocations' => $buildingLocations,
			'buildingLocationId' => $buildingLocationId,
			'sightingLocations' => $sightingLocations,
			'sightingLocationId' => $sightingLocationId,
			'notifiers' => $notifiers,
			'notifierId' => $notifierId,
			'resolutionActions' => $resolutionActions,
			'resolutionActionId' => $resolutionActionId,
			'chemicals' => $chemicals,
			'chemicalId' => $chemicalId,
//			'locationsApplied' => $locationsApplied,
//			'locationAppliedId' => $locationAppliedId,
//			'applicators' => $applicators,
//			'applicatorId' => $applicatorId,
			'submitter' => Auth::user(),
			'user' => Auth::user(),
		);

		return View::make('newissue', $params);
	}


	/**
	 * @return Response
	 */
	public function saveNewIssue()
	{
		$issueId = $this->saveIssue();
		return Redirect::to('/issue/' . $issueId);
	}

	/**
	 * @return Response
	 */
	public function saveIssue($issueId = null)
	{
		$data = Input::all();
		$send_new_issue_email = false;
		$send_issue_resolved_email = false;

		$rules = array(
			'dateSighted' => array('required', 'date'),
		);

		$validation = Validator::make($data, $rules);
//		if ($validation->fails())
//		{
//			return Redirect::to('/issue/new/' . $pestId)->with_input()->with_errors($validation);
//		}

		if ($issueId != null)
		{
			// Update issue and resolution data.
			$issue = Issue::find($issueId);
			$issue->pest_id = $data['pestId'];
			$issue->date_sighted = date('Y-m-d H:i:s', strtotime($data['dateSighted']));
			$issue->sighting_location_id = $data['sightingLocationId'];
			$issue->notified_by = $data['notifierId'];
			$issue->location_details = $data['locationDetails'];
			$issue->notes = $data['notes'];

			if (isset($data['buildingLocationId']))
			{
				$issue->location_id = $data['buildingLocationId'];
			}
			
//			if (isset($data['resolved']))
//				$issue->resolved = $data['resolved'];
			if (isset($data['status']))
				$issue->status = isset($data['status']) || $data['status'] == '' ? $data['status'] : 'submitted';
			if (isset($data['dateCompleted']))
				$issue->date_completed = date('Y-m-d', strtotime($data['dateCompleted']));
			if (isset($data['actionId']))
				$issue->action_id = $data['actionId'];
			if (isset($data['resolutionNotes']))
				$issue->resolution_notes = $data['resolutionNotes'];

			if (isset($data['expenses']))
				$issue->expenses = $data['expenses'];
			if (isset($data['chemicalId']))
				$issue->chemical_id = isset($data['chemicalId']) ? $data['chemicalId'] : 0;
			if (isset($data['dateApplied']))
				$issue->date_applied = date('Y-m-d', strtotime($data['dateApplied']));
			if (isset($data['locationApplied']))
				$issue->location_applied = isset($data['locationApplied']) ? $data['locationApplied'] : '';
			if (isset($data['applicator']))
				$issue->applicator = isset($data['applicator']) ? $data['applicator'] : '';
			if (isset($data['adminNotes']))
				$issue->admin_notes = $data['adminNotes'];
			if (isset($data['workOrder']))
				$issue->work_order = isset($data['workOrder']) ? $data['workOrder'] : '';

			$issue->save();

			// If the issue has been resolved, send an email to the originator.
			if ($issue->status == 'resolved')
				$send_issue_resolved_email = true;
		}
		else
		{
			// Retrieve the data for the logged-in user.
			$user = Auth::user();
			if (!$user)
				return Redirect::to('/login');

			// Create a new issue.
			$issueData = array(
				'pest_id' => $data['pestId'],
				'submitter_id' => $user->id,
				'location_id' => isset($data['buildingLocationId']) ? $data['buildingLocationId'] : $user->location,
				'date_sighted' => date('Y-m-d H:i:s', strtotime($data['dateSighted'])),
				'sighting_location_id' => $data['sightingLocationId'],
				'notified_by' => $data['notifierId'],
				'location_details' => $data['locationDetails'],
				'notes' => $data['notes'],
				'status' => 'submitted',
			);
			$issue = new Issue($issueData);
			$issue->save();
			$issueId = $issue->id;

			// Send an email notification about the new issue.
			$send_new_issue_email = true;
//			return $issueId;
		}

		// Save the files.
		$files = Input::file('images');
		if ($files)
		{
			foreach ($files as $file)
			{
				if ($file === null)
					continue;

				// If it's not an image, ignore the file.
				$mimetype = $file->getMimeType();
				if ($mimetype != 'image/jpeg' && $mimetype != 'image/png')
					continue;

				// Check the hash and size to see if this image has already been added.
				$path = $file->getRealPath();
				$hash = sha1_file($path);
				$filesize = $file->getSize();
				$record = Image::where('issue_id', '=', $issueId)
								->where('hash', '=', $hash)
								->where('filesize', '=', $filesize)
								->first();
				if (!$record)
				{
					// Append a timestamp to the file.
					$destFile = date('YmdHiu') . '_' . $file->getClientOriginalName();
					$destPath = Config::get('media.upload_path') . '/' . $destFile;
					$fileData = array(
						'issue_id' => $issueId,
						'hash' => $hash,
						'filename' => $destFile,
						'filesize' => $filesize,
						'sequence' => 0,
					);
					if ($file->move(Config::get('media.upload_path'), $destFile))
					{
						$fileObj = new Image($fileData);
						$fileObj->save();
					}
				}
			}
		}

		// Send the new issue notification email if we need to.
		if ($send_new_issue_email == true)
			$this->emailNewIssue($issueId);

		// Send the issue resolved notification email if we need to.
		if ($send_issue_resolved_email == true)
			$this->emailIssueResolved($issueId);
		
		return Redirect::to('/issue/' . $issueId);
	}


	/**
	 * @return Response
	 */
	public function showIssue($issueId)
	{
		// Get the pest from the issue, then the category from the pest.
		$issue = Issue::with('submitter')
					->where('id', $issueId)
					->first();
		$pest = Pest::with('images')
					->where('id', $issue->pest_id)
					->first();
		$pestId = $issue->pest_id;
		$categories = Category::lists('name', 'id');
		$categories = array_merge(array('0' => 'Please select one...'), $categories);
		$categoryId = $pest['category_id'];
		$pests = Pest::where('category_id', $categoryId)->orderBy('name')->lists('name', 'id');

		// Get the issue fields.
		$user = Auth::user();
		$buildingLocations = BuildingLocation::orderBy('name')->lists('name', 'id');
		$buildingLocationId = isset($issue->location_id) ? $issue->location_id : $user->location;

		$sightingLocations = SightingLocation::lists('name', 'id');
		$sightingLocationId = $issue->sighting_location_id;

		$notifiers = Notifier::lists('name', 'id');
		$notifierId = $issue->notified_by;

		$resolutionActions = Action::lists('name', 'id');
		$resolutionActionId = isset($issue->action_id) ? $issue->action_id : reset($resolutionActions);

		$chemicals = Chemical::orderBy('name')->lists('name', 'id');
		$chemicalId = $issue->chemical_id;

//		$locationsApplied = LocationApplied::lists('name', 'id');
//		$locationAppliedId = reset($locationsApplied);

		// Get the uploaded images.
		// TODO: This is weird, because given the models we shouldn't have to do this.
		$images = Image::where('issue_id', $issueId)->orderBy('sequence')->get();
		$pest->images = $images;

		$statuses = array('submitted' => 'Submitted',
							'in-progress' => 'In Progress',
							'resolved' => 'Resolved');

		if (isset($pest->description)) $pest->description = strip_tags($pest->description);
		if (isset($pest->remedy)) $pest->remedy = strip_tags($pest->remedy);

		$params = array(
			'categories' => $categories,
			'categoryId' => $categoryId,
			'pestId' => $pestId,
			'pests' => $pests,
			'pest' => $pest,
			'images' => $pest->images,
			'issue' => $issue,
			'buildingLocations' => $buildingLocations,
			'buildingLocationId' => $buildingLocationId,
			'sightingLocations' => $sightingLocations,
			'sightingLocationId' => $sightingLocationId,
			'notifiers' => $notifiers,
			'notifierId' => $notifierId,
			'resolutionActions' => $resolutionActions,
			'resolutionActionId' => $resolutionActionId,
			'statuses' => $statuses,
			'dateCompleted' => date('M/d/Y', strtotime($issue->date_completed)),
			'dateApplied' => date('M/d/Y', strtotime($issue->date_applied)),
			'chemicals' => $chemicals,
			'chemicalId' => $chemicalId,
//			'locationsApplied' => $locationsApplied,
//			'locationAppliedId' => $locationAppliedId,
//			'applicators' => $applicators,
//			'applicatorId' => $applicatorId,
			'workOrder' => $issue->work_order,
			'submitter' => $issue->submitter,
			'user' => Auth::user(),
		);

		return View::make('editissue', $params);
	}


	/**
	 * @return Response
	 */
	public function listPests()
	{
		$user = Auth::user();
		if ($user->access < 100)
			return Redirect::to('/');

		$pests = Pest::with('category')->orderBy('name')->get();
		$params = ['pests' => $pests];
		return View::make('manage.pestlist', $params);
	}


	/**
	 * Show the edit pest page.
	 *
	 * @return Response
	 */
	public function editPest($pestId = null)
	{
		$pest = Pest::with('category')->where('pests.id', $pestId)->first();
		$categories = Category::lists('name', 'id');
		$params = ['categories' => $categories];
		if ($pest)
		{
			$params['pest'] = $pest;
			$params['id'] = $pest->id;
			$params['name'] = $pest->name;
			$params['categoryId'] = $pest->category->id;
			$params['description'] = $pest->description;
			$params['remedy'] = $pest->remedy;
		}
		return View::make('manage.editpest', $params);
	}


	/**
	 * Show the edit pest page.
	 *
	 * @return Response
	 */
	public function savePest()
	{
		$user = Auth::user();
		if ($user->access < 100)
			return Redirect::to('/');

		// If the id already corresponds to a record in the pests table, update that entry.
		// Otherwise, generate a new record.
		$pestId = Input::get('id');
		$categoryId = Input::get('categoryId');
		$categories = Category::lists('name', 'id');
		$name = trim(Input::get('name'));
		$description = trim(Input::get('description'));
		$remedy = trim(Input::get('remedy'));

		$params = ['id' => $pestId, 'categoryId' => $categoryId, 'categories' => $categories, 'name' => $name, 'description' => $description, 'remedy' => $remedy, 'formErrors' => []];

		if ($name == '')
		{
			$params['formErrors']['name'] = 'You must enter a name for the pest.';
			return View::make('manage.editpest', $params);
		}

		if (!$categoryId || !$name)
		{
			$params['formErrors']['general'] = 'Could not save pest.';
			return View::make('manage.editpest', $params);
		}

		$pest = Pest::where('id', $pestId)->first();
		if ($pest)
		{
			// Update an existing record.
			$pest->category_id = $categoryId;
			$pest->name = $name;
			$pest->description  = $description;
			$pest->remedy = $remedy;
			$pest->save();
		}
		else
		{
			// Create a new record.
			$pest = new Pest;
			$pest->category_id = $categoryId;
			$pest->name = $name;
			$pest->description  = $description;
			$pest->remedy = $remedy;
			$pest->save();
		}

		// If a file was included with the image, process it now.
		if (isset($pest->id))
		{
			if (Input::hasFile('image'))
			{
				$oldImageFilepath = Config::get('media.pestimage_path') . '/' . $pest->imageFile;

				$file = Input::file('image');
				$filename = $file->getClientOriginalName();
				$mimetype = $file->getMimeType();
				if ($mimetype != 'image/jpeg' && $mimetype != 'image/png')
				{
					$params['id'] = $pest->id;
					$params['name'] = $pest->name;
					$params['description'] = $pest->description;
					$params['remedy'] = $pest->remedy;
					$params['formErrors']['image'] = 'You uploaded an invalid file. Please upload a JPEG or PNG.';
//					return View::make('manage.editpest', $params);
					return Redirect::to('/pest/' . $pest->id);
				}

				$target_filename = date('YmdHiu') . '_' . $filename;
				Input::file('image')->move(Config::get('media.pestimage_path'), $target_filename);
				$target_filepath =  Config::get('media.pestimage_path') . '/' . $target_filename;
				if ($this->resizeImage($target_filepath, 230))
				{
					$pest->imageFile = $target_filename;
					$pest->save();
					if (file_exists($oldImageFilepath) && !is_dir($oldImageFilepath))
						unlink($oldImageFilepath);
				}
			}	
		}
		return Redirect::to('/manage');
	}


	/**
	 * Resize an uploaded image, replacing the file.
	 */
	protected function resizeImage($imagePath, $width)
	{
		//The blur factor where >1 is blurry, <1 is sharp.
		$imagick = new \Imagick(realpath($imagePath));

		$oldWidth = $imagick->getImageWidth();
		$oldHeight = $imagick->getImageHeight();

		// Scale proportionately to the $width size.
		$newWidth = $width;
		$newHeight = $oldHeight / ($oldWidth / $newWidth);
	    if ($imagick->scaleimage($newWidth, $newHeight))
			return $imagick->writeImage($imagePath);
		return false;
/*
		if ($oldWidth > $oldHeight)
		{
			$newWidth = $width;
			$newHeight = $oldHeight / ($oldWidth / $newWidth);
		}
		else if ($oldWidth < $oldHeight
		{
			$newWidth = $width;
			$newHeight = 
		}
*/
//		$imagick->resizeImage($newWidth, $newHeight, Imagick::FILTER_LANCZOS, 0.9, true);
	}


	/**
	 * Delete a pest.
	 *
	 * @return Response
	 */
	public function deletePest($pestId)
	{
		$user = Auth::user();
		if ($user->access < 100)
			return Redirect::to('/');

		// Make sure the pest exists.
		$pest = Pest::where('id', $pestId)->first();
		if (!$pest)
			return Redirect::to('/manage');

		$params = ['pest' => $pest];
		return View::make('manage.deletepest', $params);
	}


	/**
	 * Delete a pest.
	 *
	 * @return Response
	 */
	public function processDelete()
	{
		$user = Auth::user();
		if ($user->access < 100)
			return Redirect::to('/');

		$pestId = Input::get('id');

		// Make sure the pest exists.
		$pest = Pest::where('id', $pestId)->where('deleted', 0)->first();
		if (!$pest)
			return Redirect::to('/manage');

		// If there's no issues associated with this pest, just delete the record and image.
		$issues = Issue::where('pest_id', $pest->id)->get();
		if (count($issues) == 0)
		{
			$imageFile = $pest->imageFile;
			if ($pest->delete())
			{
				// Delete the image.
				$deletePath = Config::get('media.pestimage_path') . '/' . $imageFile;
				if (file_exists($deletePath) && !is_dir($deletePath))
					unlink($deletePath);
			}
		}
		else
		{
			// Flag the pest as "deleted."
			$pest->deleted = 1;
			$pest->save();
		}

		return Redirect::to('/manage');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}