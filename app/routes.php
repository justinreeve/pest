<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Blade::extend(function($value, $compiler)
{
	if (!$value)
		return '';
	$pattern = $compiler->createMatcher('nl2br');
	return preg_replace($pattern, '$1<?php echo nl2br(e($2)); ?>', $value);
});

Route::get('/', array('uses' => 'PestController@showInfoCategory', 'before' => 'auth'));

Route::get('/login', array('uses' => 'HomeController@showLogin'));

Route::post('/login', array('uses' => 'HomeController@doLogin'));

Route::get('/logout', array('uses' => 'HomeController@doLogout'));

Route::get('/api/categories', 'PestController@getCategoriesJSON');

Route::get('/list', array('uses' => 'PestController@listIssues', 'before' => 'auth'));

Route::get('/list/{locationId}', array('uses' => 'PestController@listIssues', 'before' => 'auth'));

Route::post('/issue/new', array('uses' => 'PestController@redirectToNewIssue', 'before' => 'auth'));

Route::get('/issue/new/{pestId}', array('uses' => 'PestController@newIssue', 'before' => 'auth'));

Route::get('/issue/{issueId}', array('uses' => 'PestController@showIssue', 'before' => 'auth'));

Route::post('/issue/{issueId}', array('uses' => 'PestController@saveIssue', 'before' => 'auth'));

Route::post('/issue/new/{pestId}', function()
{
	$app = app();
	$doc = $app->make('PestController');
	$output= $doc->callAction('saveIssue', array());
	return $output;
});

Route::get('/manage', array('uses' => 'PestController@listPests', 'before' => 'auth'));

Route::get('/pest/{pestId}', array('uses' => 'PestController@editPest', 'before' => 'auth'));
Route::get('/pest', array('uses' => 'PestController@editPest', 'before' => 'auth'));
Route::post('/pest', array('uses' => 'PestController@savePest', 'before' => 'auth'));

Route::get('/deletepest/{pestId}', array('uses' => 'PestController@deletePest', 'before' => 'auth'));
Route::post('/deletepest', array('uses' => 'PestController@processDelete', 'before' => 'auth'));

Route::post('/', function()
{
	$categoryId = Input::get('categoryId');
	$pestId = Input::get('pestId');

//	Redirect::action('PestController@showInfoCategory');

	$app = app();
	$doc = $app->make('PestController');
	$output = $doc->callAction('showInfoCategory', array('categoryId' => $categoryId, 'pestId' => $pestId));
	return $output;
});

// Route::resource('pest', 'PestController');
