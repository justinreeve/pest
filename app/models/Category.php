<?php
class Category extends Eloquent
{
	protected $id;
	protected $name;

	protected $guarded = array('id');
	protected $table = 'categories';

	public function pests()
	{
		return $this->hasMany('Pest', 'category_id', 'id');
	}
}
?>