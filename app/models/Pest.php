<?php
class Pest extends Eloquent
{
	protected $id;
	protected $categoryId;
	protected $name;
	protected $description;
	protected $remedy;
	protected $imageFile;
	protected $deleted;

	protected $guarded = array('id');
	protected $table = 'pests';

	public function category()
	{
		return $this->belongsTo('Category', 'category_id', 'id');
	}

	public function images()
	{
		return $this->hasMany('Image', 'issue_id', 'id');
	}

	public function getPestImageUrl()
	{
		return Config::get('media.pestimage_www_path') . '/' . $this->imageFile;
	}

	public function getPestImagePath()
	{
		return Config::get('media.pestimage_path') . '/' . $this->imageFile;
	}
}
?>