<?php
class Image extends Eloquent
{
	protected $id;
	protected $issueId;
	protected $hash;
	protected $filename;
	protected $filesize;
	protected $sequence;

	protected $guarded = array('id');
	protected $table = 'images';

	public function issue()
	{
		return $this->belongsTo('Issue', 'issue_id', 'id');
	}
}
?>