<?php
class Resolution extends Eloquent
{
	protected $id;
	protected $issueId;
	protected $actionId;
	protected $resolutionNotes;
	protected $expenses;
	protected $chemicalId;
	protected $dateApplied;
	protected $locationApplied;
	protected $applicator;
	protected $adminNotes;
	protected $workOrder;

	protected $guarded = array('id');
	protected $table = 'resolutions';
}
?>