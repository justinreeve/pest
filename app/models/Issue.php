<?php
class Issue extends Eloquent
{
	protected $id;
	protected $pest_id;
	protected $submitter_id;
	protected $location_id;
	protected $date_sighted;
	protected $location_sighted;
	protected $notified_by;
	protected $location_details;
	protected $notes;
	protected $status;
	protected $date_completed;
	protected $action_id;
	protected $resolution_notes;
	protected $expenses;
	protected $chemical_id;
	protected $date_applied;
	protected $location_applied;
	protected $applicator;
	protected $admin_notes;
	protected $work_order;
	protected $emailed_notification;

	protected $guarded = array('id');
	protected $table = 'issues';

	public function pest()
	{
		return $this->hasOne('Pest', 'id', 'pest_id');
	}

	public function submitter()
	{
		return $this->hasOne('User', 'id', 'submitter_id');
	}

	public function action()
	{
		return $this->hasOne('Action', 'id', 'action_id');
	}

	public function chemical()
	{
		return $this->hasOne('Chemical', 'id', 'chemical_id');
	}

	public function location()
	{
		return $this->hasOne('BuildingLocation', 'id', 'location_id');
	}
/*
	public function resolution()
	{
		return $this->hasOne('Resolution', 'id', 'issue_id');
	}
*/
}
?>