<?php
class SightingLocation extends Eloquent
{
	protected $id;
	protected $name;

	protected $guarded = array('id');
	protected $table = 'sighting_locations';
}
?>