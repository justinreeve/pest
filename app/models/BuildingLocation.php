<?php
class BuildingLocation extends Eloquent
{
	protected $id;
	protected $name;
	protected $rank;

	protected $guarded = array();
	protected $table = 'building_locations';
}
?>