<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/pest.js"></script>
	</head>
	<body id="pestlist">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Pest Manager</a>
	            </div>
				<div id="headerMenu">
					<a href="/">Report Pest</a>

					<a href="/list">List</a>

					<a class="selected" href="/manage">Manage</a>

					@if (\Auth::guest())
					<a href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			<div class="addNewPest">
				<a class="button" href="/pest">Add New Pest</a>
			</div>

			<table id="pests" class="tableList tablesorter">
				<thead>
					<tr>
						<th></th>
						<th>Pest</th>
						<th>Category</th>
						<th>Description</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				@foreach ($pests as $pest)
					<tr>
						<td class="image">
						@if (file_exists(Config::get('media.pestimage_path') . '/' . $pest->imageFile) && !is_dir(Config::get('media.pestimage_path') . '/' . $pest->imageFile))
							<img src="{{ Config::get('media.pestimage_www_path') . '/' . $pest->imageFile }}" alt="" />
						@endif
						</td>
						<td class="name">
							{{ $pest->name }}
						</td>
						<td class="category">
							{{ $pest->category->name }}
						</td>
						<td class="description">
							{{ str_limit(strip_tags($pest->description), 250, '...') }}
						</td>
						<td>
							<a class="edit button" href="/pest/{{ $pest->id }}">Edit</a>
							<a class="delete button" href="/deletepest/{{ $pest->id }}">Delete</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</body>
</html>