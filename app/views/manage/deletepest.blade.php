<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/pest.js"></script>
	</head>
	<body id="deletepest">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Manage Pest</a>
	            </div>
				<div id="headerMenu">
					<a href="/">Report Pest</a>

					<a href="/list">List</a>

					<a class="selected" href="/manage">Manage</a>

					@if (\Auth::guest())
					<a href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			{{ Form::open(array('url' => '/deletepest', 'id' => 'pest_delete_form')) }}
			<div class="section-white">
				@if (isset($pest->id))
				{{ Form::hidden('id', $pest->id) }}
				@endif

				<p>Are you sure you want to delete the following pest? This action cannot be undone!</p>

				<p>Name: <strong>{{ $pest->name }}</strong></p>

				@if (isset($pest->imageFile) && file_exists(Config::get('media.pestimage_path') . '/' . $pest->imageFile))
				<img class="pestImage" src="{{ Config::get('media.pestimage_www_path') . '/' . $pest->imageFile }}" alt="" />
				@endif

				<div class="formBlock">
					<div>
						{{ Form::submit('Yes', array('class' => 'button yes')) }}
						<a class="button no" href="/manage">No</a>
					</div>
				</div>

				<div style="clear: both;"></div>
			</div>
			{{ Form:: close() }}
		</div>
	</body>
</html>