<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/pest.js"></script>
	</head>
	<body id="editpest">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Manage Pest</a>
	            </div>
				<div id="headerMenu">
					<a href="/">Report Pest</a>

					<a href="/list">List</a>

					<a class="selected" href="/manage">Manage</a>

					@if (\Auth::guest())
					<a href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			{{ Form::open(array('url' => '/pest', 'id' => 'pest_edit_form', 'files' => true)) }}
			<div class="section-white">
				@if (isset($pest->id))
				{{ Form::hidden('id', $id) }}
				@endif

				@if (isset($formErrors['general']))
				<div class="error">
					{{ $formErrors['general'] }}
				</div>
				@endif

				<div class="formBlock">
					<div class="divHeader">Pest Name</div>
					<div>
						{{ Form::input('text', 'name', isset($name) ? $name : '') }}
					</div>
					@if (isset($formErrors['name']))
					<div class="error">
						{{ $formErrors['name'] }}
					</div>
					@endif
				</div>

				<div class="formBlock">
					<div class="divHeader">Category</div>
					<div>
						{{ Form::select('categoryId', $categories, isset($categoryId) ? $categoryId : '0') }}
					</div>
					@if (isset($formErrors['category']))
					<div class="error">
						{{ $formErrors['category'] }}
					</div>
					@endif
				</div>

				<div class="formBlock">
					<div class="divHeader">Description</div>
					<div>
						{{ Form::textarea('description', isset($description) ? $description : '') }}
					</div>
					@if (isset($formErrors['description']))
					<div class="error">
						{{ $formErrors['description'] }}
					</div>
					@endif
				</div>

				<div class="formBlock">
					<div class="divHeader">Remedy</div>
					<div>
						{{ Form::textarea('remedy', isset($remedy) ? $remedy : '') }}
					</div>
					@if (isset($formErrors['remedy']))
					<div class="error">
						{{ $formErrors['remedy'] }}
					</div>
					@endif
				</div>

				<div class="formBlock">
					<div class="divHeader">Image</div>
					<div>
						<p>Please make sure the image is square (i.e. equal width and height).</p>
						@if (isset($pest->imageFile) && file_exists(Config::get('media.pestimage_path') . '/' . $pest->imageFile))
						<img class="pestImage" src="{{ Config::get('media.pestimage_www_path') . '/' . $pest->imageFile }}" alt="" />
						<br />
						@endif
						{{ Form::file('image') }}
					</div>
					@if (isset($formErrors['image']))
					<div class="error">
						{{ $formErrors['image'] }}
					</div>
					@endif
				</div>

				<div class="formBlock">
					<div>
						{{ Form::submit('Save', array('class' => 'button')) }}
						<a class="button cancel" href="/manage">Cancel</a>
					</div>
				</div>

				<div style="clear: both;"></div>
			</div>
			{{ Form:: close() }}
		</div>
	</body>
</html>