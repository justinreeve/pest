<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/pest.js"></script>
	</head>
	<body id="newpest">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Pest Sighting Report</a>
	            </div>
				<div id="headerMenu">
					<a class="selected" href="/">Report Pest</a>

					<a href="/list">List</a>

					@if (Auth::user()->access >= 100)
					<a href="/manage">Manage</a>
					@endif

					@if (Auth::guest())
					<a href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			{{ Form::open(array('id' => 'pest_info_form', 'url' => '/issue/new')) }}
			<div id="section1" class="section-white">
				<h5>Pest Information</h5>

				<div class="panelLeft">
					<div class="panelContent">
						<div class="formBlock">
							<div class="divHeader">Pest Category</div>
							<div>
								{{ Form::select('categoryId', $categories, $categoryId) }}
							</div>
						</div>

						<div id="pestBlock">
							@if(isset($categoryId) and !empty($pests))
							<div id="pestSelectBlock" class="formBlock">
								<div class="divHeader">Pest</div>
								<div>
									{{ Form::select('pestId', $pests, isset($pest->id) ? $pest->id : '0') }}
								</div>
							</div>
	
							@if(isset($pest->imageFile))
							<div id="pestImageBlock" class="formBlock">
								{{ HTML::image('images/pests/' . $pest->imageFile) }}
							</div>
							@endif
	
							<div id="pestReportBlock" class="formBlock">
								{{ HTML::link('/issue/new/' . $pest->id, 'Report This Pest', array('class' => 'fullsize button')) }}
							</div>
							@endif
						</div>
					</div>
				</div>

				<div class="panelRight">
					<div id="pestContentBlock" class="panelContent">
						@if(isset($pest))
						<h6>{{ $pest->name or '' }}</h6>
						<div class="description">
							@nl2br(strip_tags($pest->description))
						</div>
						<h6>Suggested Remedy</h6>
						<div class="remedy">
							@nl2br(strip_tags($pest->remedy))
						</div>
						@endif
					</div>
				</div>

				<div style="clear: both;"></div>
			</div>
			{{ Form::close() }}
		</div>

		<script type="text/javascript">
		$(document).ready(function()
		{
			$('#sighting-date').datepicker({
				changeMonth: true,
				changeYear: true,
			});

			$('#completion-date').datepicker({
				changeMonth: true,
				changeYear: true,
			});

			$('#chemical-date-applied').datepicker({
				changeMonth: true,
				changeYear: true,
			});
		});
		</script>
	</body>
</html>