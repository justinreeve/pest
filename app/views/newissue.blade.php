<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/pest.js"></script>
	</head>
	<body id="newissue">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Pest Sighting Report</a>
	            </div>
				<div id="headerMenu">
					<a class="selected" href="/">Report Pest</a>

					<a href="/list">List</a>

					@if (Auth::user()->access >= 100)
					<a href="/manage">Manage</a>
					@endif

					@if (Auth::guest())
					<a href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			{{ Form::open(array('id' => 'pest_info_form', 'files' => true)) }}
			<div id="section1" class="section-white">
				<h5>Pest Information</h5>

				<div class="panelLeft">
					<div class="panelContent">
						<div class="formBlock">
							<div class="divHeader">Pest Category</div>
							<div>
								{{ Form::select('categoryId', $categories, $categoryId) }}
							</div>
						</div>

						<div id="pestBlock">
							@if(isset($categoryId) and !empty($pests))
							<div id="pestSelectBlock" class="formBlock">
								<div class="divHeader">Pest</div>
								<div>
									{{ Form::select('pestId', $pests, isset($pestId) ? $pestId : '0') }}
								</div>
							</div>

							@if(isset($pest->imageFile))
							<div id="pestImageBlock" class="formBlock">
								{{ HTML::image('images/pests/' . $pest->imageFile) }}
							</div>
							@endif

							<div id="pestReportBlock" class="formBlock">
								{{ HTML::link('/issue/new/' . $pestId, 'Report This Pest', array('class' => 'fullsize button')) }}
							</div>
							@endif
						</div>
					</div>
				</div>

				@if(isset($pest))
				<div class="panelRight">
					<div id="pestContentBlock" class="panelContent">
						<h6>{{ $pest->name or '' }}</h6>
						<div class="description">
							@nl2br(strip_tags($pest->description))
						</div>
						<h6>Suggested Remedy</h6>
						<div class="remedy">
							@nl2br(strip_tags($pest->remedy))
						</div>
					</div>
				</div>
				@endif

				<div style="clear: both;"></div>
			</div>

			<div id="section2" class="section-gray">
				<h5>Report Pest</h5>
				<div class="formFieldColumns">
					<div class="formFieldCol">
						<p>Report Number:</p>
					</div>
					<div class="formFieldCol formSelect">
					@if (Auth::user()->access >= 100)
						<p>Location: {{ Form::select('buildingLocationId', $buildingLocations, $buildingLocationId) }}</p>
					@else
						<p>Location: <span>{{ (isset($submitter->location_desc) ? $submitter->location_desc : '') }}</span></p>
					@endif
					</div>
					<div class="formFieldCol">
						<p>Submitted By: <span>{{ (isset($submitter->fullname) ? $submitter->fullname : '') }}</span></p>
					</div>
					<div class="formFieldCol">
						<p>Date: <span>{{ date('m/d/Y') }}</span></p>
					</div>
					<div style="clear: both;"></div>
				</div>

				<div class="panelLeft">
					<div class="panelContent">
						<div class="formBlock">
							<div class="divHeader">Sighting Date</div>
							<div>
								{{ Form::text('dateSighted', '', array('id' => 'dateSighted')) }}
							</div>
						</div>

						<div class="formBlock">
							<div class="divHeader">Where Was Pest Seen</div>
							<div>
								{{ Form::select('sightingLocationId', $sightingLocations, $sightingLocationId) }}
							</div>
						</div>

						<div class="formBlock">
							<div class="divHeader">IPM Coordinator Notified By</div>
							<div>
								{{ Form::select('notifierId', $notifiers, $notifierId) }}
							</div>
						</div>

						<div class="formBlock">
							<div class="divHeader">Sighting Location Details</div>
							<div>
								{{ Form::textarea('locationDetails') }}
							</div>
						</div>

						<div class="formBlock">
							<div class="divHeader">Additional Notes</div>
							<div>
								{{ Form::textarea('notes') }}
							</div>
						</div>

						<div class="formBlock">
							{{ Form::submit('Submit', array('class' => 'fullsize')) }}
						</div>
					</div>
				</div>

				<div class="panelRight">
					<div class="panelContent">
						<div class="formBlock">
							<div class="divHeader">Attach Photo</div>
							<div>
								<input type="file" name="images[]" multiple="multiple" accept="image/*" />
							</div>
						</div>
					</div>
				</div>
				<div style="clear: both;"></div>
			</div>
			{{ Form::close() }}
		</div>

		<script type="text/javascript">
		$(document).ready(function()
		{
			$('#dateSighted').datepicker({
				changeMonth: true,
				changeYear: true,
			});

			$('#dateCompleted').datepicker({
				changeMonth: true,
				changeYear: true,
			});

			$('#dateApplied').datepicker({
				changeMonth: true,
				changeYear: true,
			});
		});
		</script>
	</body>
</html>