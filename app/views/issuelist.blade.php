<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="/js/themes/blue/style.css">
		<link rel="stylesheet" href="/js/jquery.tablesorter.pager.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/jquery.tablesorter.min.js"></script>
		<script src="/js/jquery.tablesorter.pager.js"></script>
	</head>
	<body id="issuelist">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Pest Issues List</a>
	            </div>
				<div id="headerMenu">
					<a href="/">Report Pest</a>

					<a class="selected" href="/list">List</a>

					@if (Auth::user()->access >= 100)
					<a href="/manage">Manage</a>
					@endif

					@if (Auth::guest())
					<a href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			@if (Auth::user()->access >= 100)
			<div class="formBlock">
				{{ Form::select('locationId', $locations, $locationId, array('id' => 'locationId')) }}
			</div>
			@endif

			<table id="issues" class="tableList tablesorter">
				<thead>
					<tr>
						<th>Pest</th>
						<th>Location</th>
						<th>Date Reported</th>
						<th>Date Sighted</th>
						<th>Resolved</th>
						<th>Date Completed</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($issues as $issue)
					<tr>
						<td>
							{{ HTML::link('/issue/' . $issue->id, isset($issue->pest->name) ? $issue->pest->name : '') }}
						</td>
						<td>
							{{ (isset($issue->location->name) ? $issue->location->name : '') }}
						</td>
						<td>
							{{ strtotime($issue->created_at) == 0 ? '' : date('m/d/Y', strtotime($issue->created_at)) }} 
						</td>
						<td>
							{{ strtotime($issue->date_sighted) == 0 ? '' : date('m/d/Y', strtotime($issue->date_sighted)) }} 
						</td>
						<td>
							@if ($issue->status == 'resolved')
							Yes
							@else
							No
							@endif
						</td>
						<td>
							@if ($issue->status == 'resolved' )
							@if (isset($issue->date_completed))
							{{ strtotime($issue->date_completed) == 0 ? '' : date('m/d/Y', strtotime($issue->date_completed)) }} 
							@else
							@endif
							@endif
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
<!--
			<div id="pager"> 
				<img src="/images/pager/first.png" class="first"/> 
				<img src="/images/pager/prev.png" class="prev"/> 
				<span class="pagedisplay"></span>
				<img src="/images/pager/next.png" class="next"/> 
				<img src="/images/pager/last.png" class="last"/> 
				<select class="pagesize" title="Select page size"> 
				    <option selected="selected" value="10">10</option> 
				    <option value="20">20</option> 
				    <option value="30">30</option> 
				    <option value="40">40</option> 
				</select>
				<select class="gotoPage" title="Select page number"></select>
			</div>
-->
		</div>
		<script>
			$(document).ready(function()
			{
				$('#issues').tablesorter({widgets: ['zebra']});

				$('select#locationId').on('change', function()
				{
					var locationId = $(this).val();
					window.location = '/list/' + locationId;
				})
			});
		</script>
	</body>
</html>