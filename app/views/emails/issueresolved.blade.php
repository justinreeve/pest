<p>
	You reported a pest on {{ date('m/d/Y', strtotime($date_created)) }}. The pest management
	team has addressed the issue, and resolved it on {{ date('m/d/Y', strtotime($date_completed)) }}.
</p>

<p>
	Pest: {{ $pest }}<br />
	Sighting Date: {{ date('m/d/Y', strtotime($date_sighted)) }}<br />
	Action Taken: {{ $action }}<br />
	Notes: {{ $resolution_notes }}
</p>