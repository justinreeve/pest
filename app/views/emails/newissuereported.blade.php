<p>
	A new issue has been created in the IPM app. Please visit
	{{ HTML::link('/issue/' . $issue_id, 'http://pest.wsd.net/issue/' . $issue_id) }}
	to see it and update the status.
</p>

<p>
	Pest: {{ $pest }}<br />
	Sighting Date: {{ date('m/d/Y', strtotime($date_sighted)) }}<br />
	Submitter: {{ $submitter }}<br />
	Location: {{ $location_desc }}
</p>