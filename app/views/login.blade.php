<!DOCTYPE html>
<html>
	<head>
		<link href="/css/Site.css" rel="stylesheet" type="text/css" />
		<link href="/css/pest.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="/js/pest.js"></script>
	</head>
	<body id="login">
		<div id="container">
			<div class="panelTitle">
				<div id="headerLogo">
					<b>Weber</b> School District
				</div>
				<div id="headerImage"></div>
			</div>
			<div class="panelHeader">
				<div class="headerTitle">
					<a href="/">Pest ID and Sighting Report</a>
	            </div>
				<div id="headerMenu">
					@if (Auth::guest())
					<a class="selected" href="/login">Login</a>
					@else
					<a href="/logout">Logout</a>
					@endif
				</div>
			</div>

			{{ Form::open(array('url' => 'login')) }}
			<div id="section1" class="section-white">
				<h5>Login</h5>
				<p>
					{{ $errors->first('username') }}
					{{ $errors->first('password') }}
					{{ (isset($ldaperrormesage) ? $ldaperrormessage: '') }}
				</p>

				<div class="panelLeft">
					<div class="panelContent">
						<div class="formBlock">
							<div class="divHeader">Username</div>
							<div>
								{{ Form::text('username', Input::old('username')) }}
							</div>
						</div>

						<div class="formBlock">
							<div class="divHeader">Password</div>
							<div>
								{{ Form::password('password') }}
							</div>
						</div>

						<div class="formBlock">
							{{ Form::submit('Submit', array('class' => '')) }}
						</div>
					</div>
				</div>
				<div style="clear: both;"></div>
			</div>
			{{ Form::close() }}
	</body>
</html>