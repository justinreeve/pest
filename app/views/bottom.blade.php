		</div>

		<script type="text/javascript">
		$('select[name="categoryId"]').on('change', function()
		{
			$('form#pest_info_form').submit();
		});
		
		$('select[name="pestId"]').on('change', function()
		{
			$('form#pest_info_form').submit();
		});

		$(document).ready(function()
		{
			$('#dateSighted').datepicker({
				changeMonth: true,
				changeYear: true,
			});

			$('#completion-date').datepicker({
				changeMonth: true,
				changeYear: true,
			});

			$('#chemical-date-applied').datepicker({
				changeMonth: true,
				changeYear: true,
			});
		});
		</script>
	</body>
</html>