## WSD Pest Management Application

The Weber School District Pest Management application allows maintenance workers to report pest sightings in their building, and site administrators to receive notifications about these pests. Images of the pests can be uploaded with each report, and a site administrator can create and add new pests for inclusion in the system.
